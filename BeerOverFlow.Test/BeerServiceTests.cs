using AutoMapper;
using BeerOverFlow.DataBase;
using BeerOverFlow.Models;
using BeerOverFlow.Services.DTOs;
using BeerOverFlow.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace BeerOverFlow.Test
{
    [TestClass]
    public class BeerServiceTests
    {

        [TestMethod]
        public void ReturnCorrectBeerDTO_When_IsValid()
        {
            //Arange
            var options = Utils.GetOptions(nameof(ReturnCorrectBeerDTO_When_IsValid));

            var beer = new Beer
            {
                Id = 1,
                Name = "Kamenitza"
            };

            using (var arrangeContext = new BeerOverFlowDbContext(options))
            {
                arrangeContext.Beers.Add(beer);
                arrangeContext.SaveChanges();
            }

            //Act&Assert

            var mapper = new Mock<IMapper>();

            mapper.Setup(x => x.Map<BeerDTO>(It.IsAny<Beer>()))
                .Returns(new BeerDTO
                {
                    Id = 1,
                    Name = "Kamenitza",
                });

            using (var actContext = new BeerOverFlowDbContext(options))
            {
                var sut = new BeerService(actContext);
                var result = sut.GetBeer(1);

                Assert.IsTrue(result.Id == beer.Id);
            }
        }


    }
}







