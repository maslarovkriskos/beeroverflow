﻿using BeerOverFlow.DataBase;
using Microsoft.EntityFrameworkCore;

namespace BeerOverFlow.Test
{
    public class Utils
    {
        public static DbContextOptions<BeerOverFlowDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<BeerOverFlowDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;

        }
    }
}
