﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeerOverFlow.Models
{
    public class Style
    {
        [Key]
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
        public string Name { get; set; }

        [Required]
        [StringLength(500,MinimumLength =10)] 
        public string Description { get; set; }
        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
    }
}
