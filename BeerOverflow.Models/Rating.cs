﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BeerOverFlow.Models
{
    public class Rating
    {
        [Key]

        public int Rate { get; set; }

        public int RateId { get; set; }

        public string IpAddress { get; set; }
        
        [ForeignKey("BeerId")]
        public int BeerId { get; set; }
        
        public virtual Beer Beer { get; set; }


    }
}
