﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace BeerOverFlow.Models
{
    public class User : IdentityUser<int>
    {
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsBanned { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsAdmin { get; set; }

       public ICollection<DrankList> DrankLists { get; set; } = new List<DrankList>(); 
        public ICollection<WishList> Wishlist { get; set; } = new List<WishList>();


    }
}
