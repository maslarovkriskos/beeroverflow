﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeerOverFlow.Models
{
    public class Beer
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public double ABV { get; set; }
        public double Rating { get; set; }

        [Required]
        [StringLength(500, MinimumLength = 10)]
        public string Description { get; set; }
        public string ImageURL { get; set; }
        public string Milliliters { get; set; }
        public bool IsDeleted { get; set; }
        public int StyleId { get; set; }
        public Style Style { get; set; }
        public int BreweryId { get; set; }
        public Brewery Brewery { get; set; }
        public ICollection<Review> Reviews { get; set; } = new List<Review>();
        public ICollection<DrankList> DrankLists { get; set; } = new List<DrankList>();
        public ICollection<WishList> WishLists { get; set; } = new List<WishList>();

    }
}
