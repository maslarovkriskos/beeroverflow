﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerOverFlow.Models
{
    public class Country
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<Brewery> Breweries { get; set; } = new List<Brewery>();
    }
}
