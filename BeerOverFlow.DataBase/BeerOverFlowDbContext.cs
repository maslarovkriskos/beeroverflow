﻿using BeerOverFlow.Models;
using BeerOverFlow.DataBase.Config;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace BeerOverFlow.DataBase
{
    public class BeerOverFlowDbContext : IdentityDbContext<User, Role, int>
    {
        public BeerOverFlowDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Beer> Beers { get; set; }
        public DbSet<Brewery> Breweries { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Style> Styles { get; set; }
        public DbSet<WishList> WishLists { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new BeerConfig());
            builder.ApplyConfiguration(new BreweryConfig());
            builder.ApplyConfiguration(new ReviewConfig());
            builder.ApplyConfiguration(new WishListConfig());
            builder.ApplyConfiguration(new DrankListConfig());

            builder.Entity<Role>().HasData(
                new Role
                {
                    Id = 1,
                    Name = "member",
                    NormalizedName = "MEMBER",
                },
                new Role
                {
                    Id = 2,
                    Name = "admin",
                    NormalizedName = "ADMIN",
                });

            var hasher = new PasswordHasher<User>();

            User admin = new User
            {
                Id = 1,
                UserName = "admin@admin.admin",
                NormalizedUserName = "ADMIN@ADMIN.ADMIN",
                Email = "admin@admin.admin",
                NormalizedEmail = "ADMIN@ADMIN.ADMIN",
                SecurityStamp = "7I5VHIJTSZNOT3KDWKNFUV5PVYBHGXN",
            };

            admin.PasswordHash = hasher.HashPassword(admin, "Admin123!");

            builder.Entity<User>().HasData(admin);

            base.OnModelCreating(builder);

            base.OnModelCreating(builder);
        }
    }
}
