﻿using BeerOverFlow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BeerOverFlow.DataBase.Config
{
    public class WishListConfig : IEntityTypeConfiguration<WishList>
    {
        public void Configure(EntityTypeBuilder<WishList> builder)
        {
            builder.HasOne(w => w.Beer)
            .WithMany(b => b.WishLists)
            .HasForeignKey(w => w.BeerId);

            builder.HasOne(w => w.User)
                .WithMany(u => u.Wishlist)
                .HasForeignKey(w => w.UserId);
        }
    }
}
