﻿using BeerOverFlow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverFlow.DataBase.Config
{
    public class DrankListConfig : IEntityTypeConfiguration<DrankList>
    {
        public void Configure(EntityTypeBuilder<DrankList> builder)
        {
            builder.HasOne(w => w.Beer)
            .WithMany(b => b.DrankLists)
            .HasForeignKey(w => w.BeerId);

            builder.HasOne(w => w.User)
                .WithMany(u => u.DrankLists)
                .HasForeignKey(w => w.UserId);
        }

    }
}
