﻿using System;
using System.Linq;
using BeerOverFlow.Models;
using BeerOverFlow.Services.DTOs;
using System.Collections.Generic;
using BeerOverFlow.Services.MapperDTos;

namespace BeerOverflow.Services.DTOMappers
{
    public static class CountryDTOMapper
    {
        public static CountryDTO GetDTO(this Country item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new CountryDTO
            {
                Id = item.Id,
                Name = item.Name,
                Breweries = item.Breweries, 
            };
        }

        public static Country GetCountry(this CountryDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new Country
            {
                Id = item.Id,
                Name = item.Name,
                Breweries = item.Breweries,
            };
        }

        public static ICollection<CountryDTO> GetDTO(this ICollection<Country> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
