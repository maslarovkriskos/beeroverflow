﻿using System;
using System.Linq;
using BeerOverFlow.Models;
using BeerOverFlow.Services.DTOs;
using System.Collections.Generic;

namespace BeerOverFlow.Services.MapperDTos
{
    public static class StyleDTOMapper
    {
        public static StyleDTO GetDTO(this Style item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new StyleDTO
            {
                Id = item.Id,
                Beers = item.Beers,
                IsDeleted = item.IsDeleted,
                Description = item.Description,
                Name = item.Name
            };
        }

        public static Style GetStyle(this StyleDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new Style
            {
                Id = item.Id,
                Beers = item.Beers,
                IsDeleted = item.IsDeleted,
                Description = item.Description,
                Name = item.Name
            };
        }

        public static ICollection<StyleDTO> GetDTO(this ICollection<Style> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
