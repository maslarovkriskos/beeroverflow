﻿using System;
using System.Linq;
using System.Text;
using BeerOverFlow.Models;
using BeerOverFlow.Services.DTOs;
using System.Collections.Generic;

namespace BeerOverFlow.Services.MapperDTos
{
    public static class BeerDTOMapper
    {
        public static BeerDTO GetDTO(this Beer item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new BeerDTO
            {
                Id= item.Id,
                Name = item.Name,
                Description = item.Description,
                Milliliters = item.Milliliters,
                ABV = item.ABV,
                ImageURL = item.ImageURL,
                Rating = item.Rating,
                IsDeleted = item.IsDeleted,
                Reviews = item.Reviews,
                Brewery = item.Brewery,
                BreweryId =item.BreweryId,
                Style = item.Style,
                StyleId = item.StyleId,
            };
        }

        public static Beer GetBeer(this BeerDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new Beer
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                Milliliters = item.Milliliters,
                ImageURL = item.ImageURL,
                ABV = item.ABV,
                Rating = item.Rating,
                IsDeleted = item.IsDeleted,
                Reviews = item.Reviews,
                Brewery = item.Brewery,
                BreweryId = item.BreweryId,
                Style = item.Style,
                StyleId = item.StyleId,
            };
        }
        public static ICollection<BeerDTO> GetDTO(this ICollection<Beer> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
