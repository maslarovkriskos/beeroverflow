﻿using BeerOverFlow.Models;
using BeerOverFlow.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverFlow.Services.MapperDTos
{
    public static class BreweryDTOMapper
    {
        public static BreweryDTO GetDTO(this Brewery item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new BreweryDTO
            {
                Id = item.Id,
                Country= item.Country,
                Name = item.Name,
                CountryId = item.CountryId,
                IsDeleted = item.IsDeleted,
                Beers = item.Beers

            };

        }

        public static Brewery GetBrewery(this BreweryDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new Brewery
            {
                Id = item.Id,
                Name = item.Name,
                Country = item.Country,
                CountryId = item.CountryId,
                IsDeleted = item.IsDeleted,
                Beers = item.Beers

            };

        }

        public static ICollection<BreweryDTO> GetDTO(this ICollection<Brewery> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
