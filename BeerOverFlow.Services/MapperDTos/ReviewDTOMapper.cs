﻿using System;
using System.Linq;
using BeerOverFlow.Models;
using BeerOverFlow.Services.DTOs;
using System.Collections.Generic;

namespace BeerOverFlow.Services.MapperDTos
{
    public static class ReviewDTOMapper
    {
        public static ReviewDTO GetDTO(this Review item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new ReviewDTO
            {
                Id = item.Id,
                BeerId = item.BeerId,
                Beer = item.Beer,
                IsDeleted = item.IsDeleted,
                Content = item.Content
            };
        }

        public static Review GetReview(this ReviewDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new Review
            {
                Id = item.Id,
                Beer = item.Beer,
                BeerId = item.BeerId,
                IsDeleted = item.IsDeleted,
                Content = item.Content
            };
        }

        public static ICollection<ReviewDTO> GetDTO(this ICollection<Review> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
