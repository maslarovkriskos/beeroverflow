﻿using BeerOverFlow.Models;
using System;
using System.Text;
using BeerOverFlow.Services.DTOs;
using System.Collections.Generic;

namespace BeerOverFlow.Services.Contracts
{
    public interface IUserService
    {
        public User BanUser(int id);

        public User DeleteUser(int id);
      
        public string GetName(int id);
        
        public User GetUser(int id);

        public UserDTO GetUser(string username);

        public ICollection<User> GetAllUsers();
       
    }
}

