﻿using System;
using System.Text;
using System.Collections.Generic;
using BeerOverFlow.Services.DTOs;
using System.Threading.Tasks;
using System.Globalization;

namespace BeerOverFlow.Services.Contracts
{
    public interface IBreweryService
    {
        BreweryDTO GetBrewery(int id);
        ICollection<BreweryDTO> GetAllBreweries();
        Task<BreweryDTO> CreateBreweryAsync(BreweryDTO breweryDTO);
        Task<BreweryDTO> UpdateBreweryAsync(int id, BreweryDTO breweryDTO);
        Task<BreweryDTO> DeleteBreweryAsync(int id);
    }
}
