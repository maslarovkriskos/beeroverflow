﻿using BeerOverFlow.Models;

namespace BeerOverFlow.Services.DTOs
{
    public class ReviewDTO
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public bool IsDeleted { get; set; }
        public Beer Beer { get; set; }
        public int BeerId { get; set; }
    }
}
