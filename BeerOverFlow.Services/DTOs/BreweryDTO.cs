﻿using BeerOverFlow.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverFlow.Services.DTOs
{
    public class BreweryDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
    }
}
