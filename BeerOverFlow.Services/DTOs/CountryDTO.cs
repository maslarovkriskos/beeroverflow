﻿using BeerOverFlow.Models;
using System.Collections.Generic;

namespace BeerOverFlow.Services.DTOs
{
    public class CountryDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public ICollection<Brewery> Breweries { get; set; } = new List<Brewery>();
    }
}