﻿using BeerOverFlow.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverFlow.Services.DTOs
{
    public class BeerDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Milliliters { get; set; }
        public string ImageURL { get; set; }
        public double Rating { get; set; }
        public double ABV { get; set; }
        public Brewery Brewery { get; set; }
        public int BreweryId { get; set; }
        public Style Style { get; set; }
        public int StyleId { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<Review> Reviews { get; set; } = new List<Review>();
    }
}
