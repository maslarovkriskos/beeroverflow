﻿using BeerOverFlow.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverFlow.Services.DTOs
{
    public class UserDTO
    {
        public DateTime CreatedOn { get; set; }
        public bool IsBanned { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsAdmin { get; set; }

        public ICollection<Review> Reviews { get; set; } = new List<Review>(); 
        public ICollection<WishList> Wishlist { get; set; } = new List<WishList>();
    }
}
