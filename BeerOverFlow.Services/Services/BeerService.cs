﻿using System;
using System.Linq;
using BeerOverFlow.Models;
using BeerOverFlow.DataBase;
using System.Collections.Generic;
using BeerOverFlow.Services.DTOs;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.MapperDTos;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BeerOverFlow.Services.Services
{
    public class BeerService : IBeerService
    {
        private readonly BeerOverFlowDbContext _context;
        public BeerService(BeerOverFlowDbContext context)
        {
            this._context = context;
        }


        public async Task<BeerDTO> CreateBeerAsync(BeerDTO beerDTO)
        {
            if (_context.Beers.Any(b => b.Name == beerDTO.Name))
            {
                var oldBeer = _context.Beers.Where(x => x.Name == beerDTO.Name).FirstOrDefault();
                _context.Beers.Remove(oldBeer);
            }

            _context.Beers.Add(beerDTO.GetBeer());

            await _context.SaveChangesAsync();

            return beerDTO;
        }

        [Authorize("")]
        public async Task<BeerDTO> DeleteBeerAsync(int id)
        {
            var beer = await this._context.Beers
                .Include(x => x.Brewery)
                .Include(x => x.Style)
                .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            beer.IsDeleted = true;

            await _context.SaveChangesAsync();

            return beer.GetDTO();
        }

        public ICollection<BeerDTO> GetAllBeers()
        {
            var beers = this._context.Beers
            .Include(b => b.Brewery)
            .Include(b => b.Style)
            .Where(b => b.IsDeleted == false)
            .Select(b => b.GetDTO())
            .ToList();

            return beers;
        }
        //Ok
        public BeerDTO GetBeer(int id)
        {
            var beer = this._context.Beers
            .Include(b => b.Brewery)
            .Include(b => b.Style)
            .FirstOrDefault(beer => beer.Id == id);

            return beer.GetDTO();
        }

        //OK
        public async Task<BeerDTO> UpdateBeerAsync(int id, BeerDTO beerDTO)
        {
            var beer = await this._context.Beers
           .Include(b => b.Brewery)
           .Include(b => b.Style)
           .Where(x => x.Id == id).FirstOrDefaultAsync();

            _context.Beers.Remove(beer);
            var newbeer = beerDTO.GetBeer();

            this._context.Beers.Add(newbeer);

            await _context.SaveChangesAsync();

            return beer.GetDTO();
        }
        //Ok
        public async Task<ICollection<BeerDTO>> FilterBeersByCountryAsync(string name)
        {
            var beers = await _context.Beers
                 .Include(b => b.Style)
                 .Include(b => b.Brewery)
                 .Where(b => b.Brewery.Country.Name == name).ToListAsync();

            return beers.GetDTO();
        }
        //Ok
        public async Task<ICollection<BeerDTO>> FilterBeersByStyleAsync(string name)
        {
            var beers = await Task.Run(() => _context.Beers
                 .Include(b => b.Style)
                 .Include(b => b.Brewery)
              .Where(b => b.Style.Name == name)
              .ToList());

            return beers.GetDTO();
        }
        //Ok
        public async Task<ICollection<BeerDTO>> SortBeerByNameAsync()
        {
            var beers = await Task.Run(() => _context.Beers
             .Include(b => b.Style)
                 .Include(b => b.Brewery)
                     .Where(b => b.IsDeleted == false)
                     .OrderBy(b => b.Name).ToList());

            return beers.GetDTO();
        }
        //Ok
        public async Task<ICollection<BeerDTO>> SortBeerByABVAsync()
        {
            var beers = await Task.Run(() => _context.Beers
             .Include(b => b.Style)
                 .Include(b => b.Brewery)
                      .Where(b => b.IsDeleted == false)
                      .OrderBy(b => b.ABV).ToList());

            return beers.GetDTO();
        }
        //Ok
        public async Task<ICollection<BeerDTO>> SortBeerByRatingAsync()
        {
            var beers = await Task.Run(() => _context.Beers
             .Include(b => b.Style)
                 .Include(b => b.Brewery)
                    .Where(b => b.IsDeleted == false)
                    .OrderByDescending(b => b.Rating).ToList());

            return beers.GetDTO();
        }

    }
}
