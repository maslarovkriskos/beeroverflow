﻿using BeerOverFlow.DataBase;
using BeerOverFlow.Models;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.DTOs;
using BeerOverFlow.Services.MapperDTos;
using BeerOverFlow.Services.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeerOverFlow.Services.Services
{
    public class UserService : IUserService
    {
        private readonly BeerOverFlowDbContext _context;
        public UserService(BeerOverFlowDbContext context)
        {
            _context = context;

        }

        public User BanUser(int id)
        {
            var user = this._context.Users
                .FirstOrDefault(u => u.Id == id);


            user.IsBanned = true;

            this._context.SaveChanges();

            return user;
        }

        public User DeleteUser(int id)
        {
            var user = this._context.Users
                .FirstOrDefault(u => u.Id == id);

            user.IsDeleted = true;

            this._context.SaveChanges();

            return user;
        }

        public string GetName(int id)
        {
            return this._context.Users.FirstOrDefault(u => u.Id == id).UserName;
        }

        public User GetUser(int id)
        {
            
            return this._context.Users.FirstOrDefault(u => u.Id == id);
        }

        public UserDTO GetUser(string username)
        {
          
            var user = this._context.Users
                .Include(u => u.Wishlist)
                .FirstOrDefault(u => u.UserName == username).GetDTO();

            return user;
        }

        public ICollection<User> GetAllUsers()
        {
            ICollection<User> users = this._context.Users.ToList();

            return users;
        }
    }
}
