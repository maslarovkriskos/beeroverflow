﻿using BeerOverFlow.DataBase;
using BeerOverFlow.Models;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.DTOs;
using BeerOverFlow.Services.MapperDTos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverFlow.Services.Services
{
    public class BreweryService : IBreweryService
    {
        private readonly BeerOverFlowDbContext _context;
        public BreweryService(BeerOverFlowDbContext context)
        {
            this._context = context;
        }

        public async Task<BreweryDTO> CreateBreweryAsync(BreweryDTO breweryDTO)
        {
            if (_context.Breweries.Any(b => b.Name == breweryDTO.Name))
            {
                var oldBrewery = _context.Breweries.Where(b => b.Name == breweryDTO.Name).FirstOrDefault();
                _context.Breweries.Remove(oldBrewery);
            }

            _context.Breweries.Add(breweryDTO.GetBrewery());
            await _context.SaveChangesAsync();

            return breweryDTO;
        }

        public async Task<BreweryDTO> DeleteBreweryAsync(int id)
        {
            var brewery = await this._context.Breweries.
                Include(x => x.Country)
                         .FirstOrDefaultAsync(x => x.Id == id);

            brewery.IsDeleted = true;

            await _context.SaveChangesAsync();

            return brewery.GetDTO();
        }

        public ICollection<BreweryDTO> GetAllBreweries()
        {
            var breweries = this._context.Breweries
                .Include(x => x.Country)
           .Where(c => c.IsDeleted == false)
           .Select(b => b.GetDTO())
           .ToList();

            return breweries;
        }

        public BreweryDTO GetBrewery(int id)
        {
            var brewery = this._context.Breweries
                .Include(x => x.Country)
                .FirstOrDefault(brewery => brewery.Id == id);

            return brewery.GetDTO();
        }

        public async Task<BreweryDTO> UpdateBreweryAsync(int id, BreweryDTO breweryDTO)
        {
            var brewery = await this._context.Breweries
            .Include(b => b.Country)
            .Where(x => x.Id == id).FirstOrDefaultAsync();

            _context.Breweries.Remove(brewery);

            var newBrewery = breweryDTO.GetBrewery();

            this._context.Breweries.Add(newBrewery);

            await _context.SaveChangesAsync();

            return brewery.GetDTO();
        }
    }
}


