﻿using BeerOverFlow.DataBase;
using BeerOverFlow.Models;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.DTOs;
using BeerOverFlow.Services.MapperDTos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverFlow.Services.Services
{
    public class ReviewService : IReviewService
    {
        private readonly BeerOverFlowDbContext _context;

        public ReviewService(BeerOverFlowDbContext context)
        {
            this._context = context;
        }

        public async Task<ReviewDTO> CreateReviewAsync(ReviewDTO reviewDTO)
        {
            if (_context.Reviews.Any(b => b.BeerId == reviewDTO.BeerId))
            {
                var oldReview = _context.Reviews.Where(b => b.BeerId == reviewDTO.BeerId).FirstOrDefault();
                _context.Reviews.Remove(oldReview);
            }
            _context.Reviews.Add(reviewDTO.GetReview());

            await _context.SaveChangesAsync();

            return reviewDTO;
        }

        public async Task<ReviewDTO> DeleteReviewAsync(int id)
        {

            var review = await Task.Run(() => this._context.Reviews
                 .Include(r => r.Beer)
                 .Where(x => x.BeerId == id).FirstOrDefault());

            review.IsDeleted = true;

            await _context.SaveChangesAsync();

            return review.GetDTO();
        }
        public async Task<ICollection<ReviewDTO>> GetAllReviewsAsync()
        {
            var reviews = await Task.Run(() => this._context.Reviews
           .Include(b => b.Beer)
           .Where(b => b.IsDeleted == false)
           .Select(b => b.GetDTO())
           .ToListAsync());

            return reviews;
        }
        public async Task<ReviewDTO> GetReviewAsync(int id)
        {
            var review = await Task.Run(() => this._context.Reviews
                 .FirstOrDefaultAsync(r => r.Id == id));

            return review.GetDTO();
        }

        public async Task<ReviewDTO> UpdateReviewAsync(int id, ReviewDTO reviewDTO)
        {
            var review = await Task.Run(() => this._context.Reviews
                .Where(x => x.Id == id).FirstOrDefault());

            review = reviewDTO.GetReview();

            await _context.SaveChangesAsync();

            return review.GetDTO();
        }
    }
}
