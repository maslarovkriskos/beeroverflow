﻿using BeerOverFlow.Models;

namespace BeerOverFlow.Web.Models
{
    public class ReviewViewModel
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public bool IsDeleted { get; set; }
        public int BeerId { get; set; }
        public Beer Beer { get; set; }
    }
}
