﻿using BeerOverFlow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverFlow.Web.Models
{
    public class BeerViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Milliliters { get; set; }
        public double Rating { get; set; }
        public string ABV { get; set; }
        public string ImageURL { get; set; }
        public Brewery Brewery { get; set; }
        public int BreweryId { get; set; }
        public Style Style { get; set; }
        public int StyleId { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<Review> Reviews { get; set; } = new List<Review>();

    }
}
