﻿using BeerOverFlow.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverFlow.Web.Models
{
    public class FilterViewModel
    {
        public string SearchText { get; set; }
        public IEnumerable<BeerDTO> SearchResults { get; set; }
        public string SearchBeer { get; set; }
        public BeerDTO BeerDTO { get; set; }
    }
}
