using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using BeerOverFlow.DataBase;
using Microsoft.EntityFrameworkCore;
using BeerOverFlow.Models;
using Microsoft.AspNetCore.Identity;
using BeerOverFlow.Services.Contracts;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Query;
using BeerOverFlow.Services.Services;
using Microsoft.AspNetCore.Identity.UI.Services;
using BeerOverFlow.Web.Areas.Identity.Pages;

namespace BeerOverFlow.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<BeerOverFlowDbContext>
                (options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                b => b.MigrationsAssembly("BeerOverFlow.Web")));

            services.AddControllersWithViews();

            services.AddIdentity<User, Role>()
               .AddEntityFrameworkStores<BeerOverFlowDbContext>()
               .AddDefaultTokenProviders();

            services.AddAutoMapper(typeof(Startup));
            services.AddSingleton<IEmailSender, EmailSender>();
            services.AddRazorPages();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IBeerService, BeerService>();
            services.AddScoped<IBreweryService, BreweryService>();
            services.AddScoped<IStyleService, StyleService>();
            services.AddScoped<IReviewService, ReviewService>();



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

        }
    }
}
