﻿using BeerOverFlow.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Web.ApiControllers.Contracts
{
    public interface IBeerApiController
    {
        [HttpPost("")]
        Task<IActionResult> CreateBeer([FromBody] BeerDTO beerDTO);

        [HttpDelete("{name}")]
        Task<IActionResult> DeleteBeer(int id);

        [HttpGet("")]
       IActionResult GetAllBeers();

        [HttpGet("{id}")]
        IActionResult GetBeer(int id);

        [HttpPut("")]
        Task<IActionResult> UpdateBeer(int id, BeerDTO beerDTO);

        [HttpGet("sortbyname")]
        Task<IActionResult> SortByName();

        [HttpGet("sortbyabv")]
        Task<IActionResult> SortByAbv();

        [HttpGet("sortbyrating")]
        Task<IActionResult> SortByRating();

        [HttpGet("filterbycountry")]
        Task<IActionResult> FilterBeersByCountry(string name);

        [HttpGet("filterbystyle")]
        Task<IActionResult> FilterBeersByStyle(string name);
    }
}
