﻿using BeerOverFlow.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverFlow.Web.API_Controllers.Contracts
{
    public interface IReviewAPIController
    {
        [HttpPost("")]
        Task<IActionResult> CreateReview([FromBody] ReviewDTO reviewDTO);

        [HttpDelete("{name}")]
        Task<IActionResult> DeleteReview(int id);

        [HttpGet("")]
        Task<IActionResult> GetAllReviews();

        [HttpGet("{id}")]

        Task<IActionResult> GetReview(int id);

        [HttpPut("")]
        Task<IActionResult> UpdateReview(int id, [FromBody] ReviewDTO reviewDTO);
    }
}
