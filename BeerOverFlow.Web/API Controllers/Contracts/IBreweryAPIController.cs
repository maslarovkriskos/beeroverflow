﻿using BeerOverFlow.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverFlow.Web.API_Controllers.Contracts
{
    public interface IBreweryAPIController
    {
        [HttpPost("")]
        Task<IActionResult> CreateBrewery([FromBody] BreweryDTO breweryDTO);

        [HttpDelete("{name}")]
        Task<IActionResult> DeleteBrewery(int id);

        [HttpGet("")]
        IActionResult GetAllBreweries();

        [HttpGet("{id}")]

       IActionResult GetBrewery(int id);

        [HttpPut("")]
        Task<IActionResult> UpdateBrewery([FromQuery] int id, [FromQuery] BreweryDTO breweryDTO);
    }
}
