﻿using BeerOverFlow.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverFlow.Web.API_Controllers.Contracts
{
    public interface ICountryAPIController
    {
        [HttpPost("")]
        Task<IActionResult> CreateCountry([FromBody]CountryDTO countryDTO);

        [HttpDelete("{name}")]
        Task<IActionResult> DeleteCountry(int id);

        [HttpGet("")]
        Task<IActionResult> GetAllCountries();

        [HttpGet("{id}")]

        Task<IActionResult> GetCountry(int id);

        [HttpPut("")]
        Task<IActionResult> UpdateCountry([FromQuery]int id, [FromQuery]CountryDTO countryDTO);
    }
}
