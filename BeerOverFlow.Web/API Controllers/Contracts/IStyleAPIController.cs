﻿using BeerOverFlow.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverFlow.Web.API_Controllers.Contracts
{
    public interface IStyleAPIController
    {
        [HttpPost("")]
        Task<IActionResult> CreateStyle([FromBody] StyleDTO styleDTO);

        [HttpDelete("{name}")]
        Task<IActionResult> DeleteStyle(string name);

        [HttpGet("")]
        Task<IActionResult> GetAllStyles();

        [HttpGet("{id}")]
        Task<IActionResult> GetStyle(int id);

        [HttpPut("")]
        Task<IActionResult> UpdateStyle([FromQuery] string oldname, [FromQuery] string newName);
    }
}
