﻿using AutoMapper;
using BeerOverFlow.Models;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.DTOs;
using BeerOverFlow.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

public class CountriesController : Controller
{
    private readonly ICountryService _service;
    private readonly IMapper _mapper;
    private readonly SignInManager<User> _signInManager;

    public CountriesController(SignInManager<User> signInManager, ICountryService service, IMapper mapper)
    {
        _service = service;
        _mapper = mapper;
        _signInManager = signInManager;
    }

    // GET: Countries
    public async Task<IActionResult> Index()
    {
        return View(await _service.GetAllCountriesAsync());
    }

    // GET: Countries/Details/5
    public async Task<IActionResult> Details(int id)
    {
        var country = await _service.GetCountryAsync(id);

        return View(country);
    }

    // GET: Countries/Create
    public IActionResult Create()
    {
        if (!_signInManager.IsSignedIn(User))
        {
            Response.Redirect("http://localhost:53299/Identity/Account/Login");
        }
        return View();
    }

    // POST: Countries/Create
    [Authorize]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(CountryViewModel countryViewModel)
    {
        var countryDTO = _mapper.Map<CountryDTO>(countryViewModel);

        await _service.CreateCountryAsync(countryDTO);

        return RedirectToAction(nameof(Index));
    }

    // GET: Countries/Edit/5
    public async Task<IActionResult> Edit(int id)
    {
        if (!_signInManager.IsSignedIn(User))
        {
            Response.Redirect("http://localhost:53299/Identity/Account/Login");
        }
        var country = await _service.GetCountryAsync(id);
        return View(country);
    }

    [Authorize]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, CountryViewModel countryViewModel)
    {
        var countryDTO = _mapper.Map<CountryDTO>(countryViewModel);

        await _service.UpdateCountryAsync(id, countryDTO);

        return RedirectToAction(nameof(Index));
    }

    // GET: Countries/Delete/5
    public async Task<IActionResult> Delete(int id)
    {
        if (!_signInManager.IsSignedIn(User))
        {
            Response.Redirect("http://localhost:53299/Identity/Account/Login");
        }

        var country = await _service.GetCountryAsync(id);

        return View(country);
    }

    // POST: Countries/Delete/5
    [Authorize]
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        var country = await _service.DeleteCountryAsync(id);

        return RedirectToAction(nameof(Index));
    }
}