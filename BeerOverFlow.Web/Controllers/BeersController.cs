﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using BeerOverFlow.Services.Contracts;
using AutoMapper;
using BeerOverFlow.Web.Models;
using BeerOverFlow.Services.DTOs;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using BeerOverFlow.Models;
using Microsoft.AspNetCore.Identity;

namespace BeerOverFlow.Web.Controllers
{
    public class BeersController : Controller
    {
        private readonly IBeerService _beerService;
        private readonly IMapper _mapper;
        private readonly IStyleService _styleService;
        private readonly IBreweryService _breweryService;
        private readonly ICountryService _countryService;
        private readonly SignInManager<User> _signInManager;

        public BeersController(SignInManager<User> signInManager, IBeerService service, IMapper mapper, IBreweryService breweryService, IStyleService styleService, ICountryService countryService)
        {
            this._beerService = service;
            this._mapper = mapper;
            this._breweryService = breweryService;
            this._styleService = styleService;
            this._countryService = countryService;
            this._signInManager = signInManager;
        }

        public IActionResult Index()
        {
            return View( _beerService.GetAllBeers());
        }

        [HttpGet]
        public IActionResult FilterByCountry()
        {
            var vm = new FilterViewModel();
            vm.SearchText = "";
            vm.SearchResults = new List<BeerDTO>();
            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> FilterByCountry(FilterViewModel model)
        {
            var result = await _beerService.FilterBeersByCountryAsync(model.SearchText);
            model.SearchResults = result;
            return View(model);
        }

        [HttpGet]
        public IActionResult FilterByStyle()
        {
            var vm = new FilterViewModel();
            vm.SearchText = "";
            vm.SearchResults = new List<BeerDTO>();
            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> FilterByStyle(FilterViewModel model)
        {
            var result = await _beerService.FilterBeersByStyleAsync(model.SearchText);
            model.SearchResults = result;
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> SortByABV()
        {
            var result = await _beerService.SortBeerByABVAsync();
            return View(result);
        }

        [HttpGet]
        public async Task<IActionResult> SortByName()
        {
            var result = await _beerService.SortBeerByNameAsync();
            return View(result);
        }

        [HttpGet]
        public async Task<IActionResult> SortByRating()
        {
            var result = await _beerService.SortBeerByRatingAsync();
            return View(result);
        }

        public IActionResult Details(int id)
        {
            var beer =  _beerService.GetBeer(id);
            return View(beer);
        }

        public IActionResult Create()
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("http://localhost:53299/Identity/Account/Login");
            }
            ViewData["BreweryId"] = new SelectList( _breweryService.GetAllBreweries(), "Id", "Name");
            ViewData["StyleId"] = new SelectList( _styleService.GetAllStyles(), "Id", "Name");
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BeerViewModel beer)
        {

            var beerDTO = _mapper.Map<BeerDTO>(beer);

            await _beerService.CreateBeerAsync(beerDTO);

            return RedirectToAction(nameof(Index));
        }


        public IActionResult Edit(int id)
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("http://localhost:53299/Identity/Account/Login");
            }

            var beer =  _beerService.GetBeer(id);

            ViewData["BreweryId"] = new SelectList( _breweryService.GetAllBreweries(), "Id", "Name");
            ViewData["StyleId"] = new SelectList( _styleService.GetAllStyles(), "Id", "Name");

            return View(beer);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, BeerViewModel beerViewModel)
        {
            var beerDTO = _mapper.Map<BeerDTO>(beerViewModel);

            await _beerService.UpdateBeerAsync(id, beerDTO);

            return RedirectToAction(nameof(Index));
        }



        public IActionResult Delete(int id)
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("http://localhost:53299/Identity/Account/Login");
            }

            var beer =  _beerService.GetBeer(id);

            return View(beer);
        }


        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var beer = await _beerService.DeleteBeerAsync(id);

            return RedirectToAction(nameof(Index));
        }
      
    }
}
