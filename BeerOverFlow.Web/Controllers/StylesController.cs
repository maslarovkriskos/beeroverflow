﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BeerOverFlow.DataBase;
using BeerOverFlow.Models;
using AutoMapper;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Web.Models;
using BeerOverFlow.Services.DTOs;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace BeerOverFlow.Web.Controllers
{
    public class StylesController : Controller
    {
        private readonly IStyleService _service;
        private readonly IMapper _mapper;
        private readonly SignInManager<User> _signInManager;

        public StylesController(SignInManager<User> signInManager, IStyleService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
            _signInManager = signInManager;
        }

        // GET: Styles
        public IActionResult Index()
        {
            return View( _service.GetAllStyles());
        }

        // GET: Styles/Details/5
        public IActionResult Details(int id)
        {
            var style = _service.GetStyle(id);

            return View(style);
        }

        // GET: Styles/Create
        public IActionResult Create()
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("http://localhost:53299/Identity/Account/Login");
            }

            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(StyleViewModel styleViewModel)
        {
            var styleDTO = _mapper.Map<StyleDTO>(styleViewModel);

            await _service.CreateStyleAsync(styleDTO);

            return RedirectToAction(nameof(Index));
        }

        // GET: Styles/Edit/5
        public IActionResult Edit(int id)
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("http://localhost:53299/Identity/Account/Login");
            }

            var style = _service.GetStyle(id);
            return View(style);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, StyleViewModel styleViewModel)
        {
            var styleDTO = _mapper.Map<StyleDTO>(styleViewModel);

            await _service.UpdateStyleAsync(id, styleDTO);

            return RedirectToAction(nameof(Index));
        }

        // GET: Styles/Delete/5
        public IActionResult Delete(int id)
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("http://localhost:53299/Identity/Account/Login");
            }

            var style =  _service.GetStyle(id);

            return View(style);
        }

        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var country = await _service.DeleteStyleAsync(id);

            return RedirectToAction(nameof(Index));
        }
    }
}
