﻿using AutoMapper;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Web.Models;
using BeerOverFlow.Services.DTOs;
using Microsoft.AspNetCore.Identity;
using BeerOverFlow.Models;
using Microsoft.AspNetCore.Authorization;

namespace BeerOverFlow.Web.Controllers
{
    public class ReviewsController : Controller
    {
        private readonly IBeerService _beerService;
        private readonly IReviewService _service;
        private readonly IMapper _mapper;
        private readonly SignInManager<User> _signInManager;

        public ReviewsController(SignInManager<User> signInManager, IBeerService beerService, IReviewService service, IMapper mapper)
        {
            _beerService = beerService;
            _service = service;
            _mapper = mapper;
            _signInManager = signInManager;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _service.GetAllReviewsAsync());
        }

        public async Task<IActionResult> Details(int id)
        {
            var review = await _service.GetReviewAsync(id);

            return View(review);
        }

        public IActionResult Create()
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("http://localhost:53299/Identity/Account/Login");
            }

            ViewData["BeerId"] = new SelectList( _beerService.GetAllBeers(), "Id", "Name");
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ReviewViewModel reviewViewModel)
        {
            var reviewDTO = _mapper.Map<ReviewDTO>(reviewViewModel);

            await _service.CreateReviewAsync(reviewDTO);

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int id)
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("http://localhost:53299/Identity/Account/Login");
            }

            var review = await _service.GetReviewAsync(id);

            ViewData["BeerId"] = new SelectList(_beerService.GetAllBeers(), "Id", "Name");

            return View(review);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ReviewViewModel reviewViewModel)
        {
            var reviewDTO = _mapper.Map<ReviewDTO>(reviewViewModel);

            await _service.UpdateReviewAsync(id, reviewDTO);

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Delete(int id)
        {
            if (!_signInManager.IsSignedIn(User))
            {
                Response.Redirect("http://localhost:53299/Identity/Account/Login");
            }

            var review = await _service.GetReviewAsync(id);

            return View(review);
        }

        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var review = await _service.DeleteReviewAsync(id);

            return RedirectToAction(nameof(Index));
        }
    }
}
